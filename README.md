Se você tem alguma dúvida sobre o GitLab, pode conferir os tutoriais abaixo.

Tutorial de operações básicas do GitLab:
https://www.youtube.com/watch?v=BAryvCEC9AY

Tutorial de uso do GitLab com o NetBeans:
https://www.youtube.com/watch?v=-6z_wnuCakc

Documento oficial do NetBeans de suporte ao git:
https://netbeans.org/kb/docs/ide/git_pt_BR.html

Resolver conflitos de merge manualmente:
https://git-scm.com/book/pt-br/v1/Ramifica%C3%A7%C3%A3o-Branching-no-Git-B%C3%A1sico-de-Branch-e-Merge