/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import model.Pessoa;

/**
 *
 * @author Rafael Rutsatz
 */
@XmlRootElement(name = "objetos")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlImportacao {

    private static final String arquivoImport = "importar.xml";

    @XmlElementWrapper(name = "pessoas")
    @XmlElement(name = "pessoa")
    private List<Pessoa> pessoas = new ArrayList<>();

    public XmlImportacao() {
    }

    public static Boolean addItem(Object obj) {
        JAXBContext context;
        Unmarshaller unMarshal = null;
        FileWriter writer = null;
        try {
            context = JAXBContext.newInstance(XmlImportacao.class);
            unMarshal = context.createUnmarshaller();
            XmlImportacao xml = (XmlImportacao) unMarshal.unmarshal(new File(arquivoImport));

            xml.get(obj).add(obj);

            return salvar(xml);

        } catch (JAXBException ex) {
            Logger.getLogger(XmlImportacao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public static Boolean salvar(XmlImportacao xml) {
        JAXBContext context = null;
        Marshaller marshal = null;
        FileWriter writer = null;
        try {
            context = JAXBContext.newInstance(XmlImportacao.class);
            marshal = context.createMarshaller();
            writer = new FileWriter(arquivoImport);
            marshal.marshal(xml, writer);

        } catch (JAXBException ex) {
            Logger.getLogger(JAXB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(JAXB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public List get(Object obj) {
        if (obj.getClass().isInstance(Pessoa.class)) {
            return this.pessoas;
//        }else{
//            return this.pessoas;
        }                
        System.err.println("Classe no arquivo xml não mapeada.");
        return null;
    }

}
