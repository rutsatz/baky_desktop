package model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public  class Funcionario extends Pessoa{
    
    @OneToOne
    @JoinColumn(name = "id_Funcao",foreignKey = @ForeignKey(name = "id_Funcao_fk"))
    private Funcao funcao;
    
    public Funcionario() {

    }


}
