package model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parametro {

    @Id
    @Column(name = "id_Parametro")
    private Integer idParametro;

    /**
     * @return the idParametro
     */
    public Integer getIdParametro() {
        return idParametro;
    }

    /**
     * @param idParametro the idParametro to set
     */
    public void setIdParametro(Integer idParametro) {
        this.idParametro = idParametro;
    }

}
