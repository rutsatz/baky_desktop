package model;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

//import javafx.beans.property.SimpleStringProperty;

@Entity
//@Access(AccessType.PROPERTY)
@SequenceGenerator(name = "idMenuSeq", sequenceName = "id_Menu_Seq", allocationSize = 1)
public class Menu {

	@Id
	@Column(name = "id_Menu")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idMenuSeq")
	private Integer idMenu;

	@ManyToMany
	@JoinTable(name = "Perfil_Menu", foreignKey = @ForeignKey(name = "id_Perfil_FK"), joinColumns = @JoinColumn(name = "id_Menu"), inverseJoinColumns = @JoinColumn(name = "id_Perfil"))
	private List<Perfil> perfis;

	@ManyToMany
	@JoinTable(name = "Usuario_Menu", foreignKey = @ForeignKey(name = "id_Menu_FK"), joinColumns = @JoinColumn(name = "id_Menu"), inverseJoinColumns = @JoinColumn(name = "id_Usuario"))
	private List<Usuario> usuarios;

	// Auto- relacionamento
	@OneToMany(mappedBy = "idMenuPai")
	private List<Menu> subMenus;

	@ManyToOne
	@JoinColumn(name = "id_Menu_Pai", foreignKey = @ForeignKey(name = "id_Menu_FK"))
	private Menu idMenuPai;

	@Column(name="tem_Sub_Menu")
	private Boolean temSubMenu = true;
	
	@Column(nullable = false, length = 60)
	private String acao;

	@Column(length = 100, nullable = false)
	private String descr;

	@Column(nullable = false)
	private String arquivo;

	@Column(name="Nivel_Sub_Menu")
	private Integer nivelSubMenu;
	
	public Menu() {
	}

	/**
	 * Cadastro de menus da aplicacao.
	 * 
	 * @param descr
	 *            Descricao do menu.
	 * @param acao
	 *            Cadastro da acao a ser executada.
	 */
	public Menu(String descr, String acao, String arquivo, Boolean temSubMenu, Integer nivel) {
		this.descr = descr; // @@
		
		this.acao = acao;
		this.arquivo = arquivo;
		this.temSubMenu = temSubMenu;
		this.nivelSubMenu = nivel;
	}

	/**
	 * Cadastro de menus da aplica��o.
	 * 
	 * @param descr
	 *            Descri��o do menu.
	 * @param acao
	 *            Cadastro da a��o a ser executada.
	 * @param idMenuPai
	 *            Menu pai.
	 */
	public Menu(String descr, String acao, String arquivo, Boolean temSubMenu, Integer nivel, Menu idMenuPai) {
		this.descr = descr; // @@
		this.acao = acao;
		this.arquivo = arquivo;
		this.temSubMenu = temSubMenu;
		this.nivelSubMenu = nivel;
		this.idMenuPai = idMenuPai;
	}

	@Override
	public String toString() {
		String str;
		str = "IdMenu = " + this.idMenu.toString() + ", ";
		if (this.idMenuPai != null)
			str += "IdMenuPai " + this.idMenuPai.toString() + " ";
		str += "descr = " + this.descr;
		return str;
	}

	public Integer getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Menu> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<Menu> subMenus) {
		this.subMenus = subMenus;
	}

	public Menu getIdMenuPai() {
		return idMenuPai;
	}

	public void setIdMenuPai(Menu idMenuPai) {
		this.idMenuPai = idMenuPai;
	}

	public Boolean getTemSubMenu() {
		return temSubMenu;
	}

	public void setTemSubMenu(Boolean temSubMenu) {
		this.temSubMenu = temSubMenu;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getDescr() { // @@
		return descr;
	}
	
//	public String getDescr(){
//		return descr.get();
//	}

	public void setDescr(String descr) { // @@
		this.descr = descr;
	}
	
//	public void setDescr(String descr) {
//		this.descr.set(descr);		
//	}

	public Integer getNivelSubMenu() {
		return nivelSubMenu;
	}

	public void setNivelSubMenu(Integer nivelSubMenu) {
		this.nivelSubMenu = nivelSubMenu;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
}
