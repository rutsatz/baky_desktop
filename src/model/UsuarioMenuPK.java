package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

@Embeddable
public class UsuarioMenuPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "id_Usuario")
	@JoinColumn(name = "id_Usuario")
	private Integer idUsuario;

	@Column(name = "id_Menu")
	@JoinColumn(name = "id_Menu")
	private Integer idMenu;

	public UsuarioMenuPK() {
	}

	public UsuarioMenuPK(Integer idUsuario, Integer idMenu) {
		super();
		this.idUsuario = idUsuario;
		this.idMenu = idMenu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMenu;
		result = prime * result + idUsuario;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioMenuPK other = (UsuarioMenuPK) obj;
		if (idMenu != other.idMenu)
			return false;
		if (idUsuario != other.idUsuario)
			return false;
		return true;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}

}
