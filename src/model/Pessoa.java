/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import utils.LocalDateAdapter;

/**
 *
 * @author rutsa
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(name = "idPessoaSeq", sequenceName = "id_Pessoa_Seq", allocationSize = 1)
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"cpf"}, name = "pessoa_cpf_uk")})
public class Pessoa {

    @Id
    @Column(name = "id_Pessoa")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idPessoaSeq")
    private Integer idPessoa;

    @XmlElementWrapper(name = "enderecos")
    @XmlElement(name = "endereco")

    @OneToMany
    @JoinColumn(name = "id_Pessoa", foreignKey = @ForeignKey(name = "id_Pessoa_fk"))
    private List<Endereco> enderecos = new ArrayList<>();

    @XmlElementWrapper(name = "telefones")
    @XmlElement(name = "telefone")

    @OneToMany
    @JoinColumn(name = "id_Telefone", foreignKey = @ForeignKey(name = "id_Telefone_fk"))
    private List<Telefone> telefones = new ArrayList<>();

    @Column(nullable = false, length = 60)
    private String nome;

    @XmlElement(name = "dataNascimento")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)

    @Column(name = "data_Nascimento")
    private LocalDate dataNascimento;

    @Column(nullable = false, length = 11)
    private String CPF;

    @Column(length = 10)
    private String RG;

    @Column(length = 14)
    private String CNPJ;

    @Column(name = "ck_fisica")
    private Boolean ck_fisica;

    /**
     * Versão do registro.
     */
    @Column(name="serie_rep",nullable = false)
    private Integer serieRep;

    /**
     * Chave primária no servidor.
     */
    @Column(name="id_Ext")
    private Integer idExt;
    
    @Version
    @Column(name = "ultima_Alteracao")
    private LocalDateTime ultimaAlteracao;

    public Pessoa() {

    }

    @Override
    public String toString() {
        String str;
        str = "Produtor: Nome = " + this.nome + ", "
                + "CPF = " + this.CPF;

        return str;
    }

    public Integer getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getRG() {
        return RG;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public LocalDateTime getUltimaAlteracao() {
        return ultimaAlteracao;
    }

    public void setUltimaAlteracao(LocalDateTime ultimaAlteracao) {
        this.ultimaAlteracao = ultimaAlteracao;
    }

    /**
     * @return the CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }

    /**
     * @param CNPJ the CNPJ to set
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    /**
     * @return the ck_fisica
     */
    public Boolean getCk_fisica() {
        return ck_fisica;
    }

    /**
     * @param ck_fisica the ck_fisica to set
     */
    public void setCk_fisica(Boolean ck_fisica) {
        this.ck_fisica = ck_fisica;
    }

    /**
     * @return the enderecos
     */
    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    /**
     * @param enderecos the enderecos to set
     */
    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    /**
     * @return the telefones
     */
    public List<Telefone> getTelefones() {
        return telefones;
    }

    /**
     * @param telefones the telefones to set
     */
    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    /**
     * @return the serieRep
     */
    public Integer getSerieRep() {
        return serieRep;
    }

    /**
     * @param serieRep the serieRep to set
     */
    public void setSerieRep(Integer serieRep) {
        this.serieRep = serieRep;
    }

    /**
     * @return the idExt
     */
    public Integer getIdExt() {
        return idExt;
    }

    /**
     * @param idExt the idExt to set
     */
    public void setIdExt(Integer idExt) {
        this.idExt = idExt;
    }
}
