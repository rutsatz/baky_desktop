package model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Usuario_Menu")
public class UsuarioMenu {

	@EmbeddedId
	private UsuarioMenuPK usuarioMenuPK;

	public UsuarioMenu() {
	}

	public UsuarioMenuPK getUsuarioMenuPK() {
		return usuarioMenuPK;
	}

	public void setUsuarioMenuPK(UsuarioMenuPK usuarioMenuPK) {
		this.usuarioMenuPK = usuarioMenuPK;
	}

}
