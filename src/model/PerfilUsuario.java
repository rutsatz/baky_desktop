package model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Perfil_Usuario", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id_Usuario", "id_Perfil" }, name = "Perfil_Usuario_uk") })
public class PerfilUsuario {

	@EmbeddedId
	private PerfilUsuarioPK perfilUsuarioPK;

	public PerfilUsuario() {
	}

	public PerfilUsuario(PerfilUsuarioPK perfilUsuarioPK) {
		this.perfilUsuarioPK = perfilUsuarioPK;
	}

	public PerfilUsuarioPK getPerfilUsuarioPK() {
		return perfilUsuarioPK;
	}

	public void setPerfilUsuarioPK(PerfilUsuarioPK perfilUsuarioPK) {
		this.perfilUsuarioPK = perfilUsuarioPK;
	}

}
