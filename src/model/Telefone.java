/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author rutsa
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@SequenceGenerator(name = "idTelefoneSeq", sequenceName = "id_Telefone_Seq", allocationSize = 1)
public class Telefone {

    @Id
    @Column(name = "id_Telefone")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idTelefoneSeq")
    private Integer idTelefone;
    
    @Column(length = 2)
    private Integer DDD;
    
    @Column(length = 10)
    private Integer telefone;

    /**
     * @return the idTelefone
     */
    public Integer getIdTelefone() {
        return idTelefone;
    }

    /**
     * @param idTelefone the idTelefone to set
     */
    public void setIdTelefone(Integer idTelefone) {
        this.idTelefone = idTelefone;
    }

    /**
     * @return the DDD
     */
    public Integer getDDD() {
        return DDD;
    }

    /**
     * @param DDD the DDD to set
     */
    public void setDDD(Integer DDD) {
        this.DDD = DDD;
    }

    /**
     * @return the telefone
     */
    public Integer getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }
}
