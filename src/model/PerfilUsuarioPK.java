package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

@Embeddable
public class PerfilUsuarioPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "id_Usuario")
	@JoinColumn(name = "id_Usuario")
	private Integer idUsuario;

	@Column(name = "id_Perfil")
	@JoinColumn(name = "id_Perfil")
	private Integer idPerfil;

	public PerfilUsuarioPK() {
	}

	public PerfilUsuarioPK(int idUsuario, int idPerfil) {
		super();
		this.idUsuario = idUsuario;
		this.idPerfil = idPerfil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPerfil;
		result = prime * result + idUsuario;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilUsuarioPK other = (PerfilUsuarioPK) obj;
		if (idPerfil != other.idPerfil)
			return false;
		if (idUsuario != other.idUsuario)
			return false;
		return true;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}

}
