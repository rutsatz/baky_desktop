package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "idPerfilSeq", sequenceName = "id_Perfil_Seq", allocationSize = 1)
public class Perfil {

	@Id
	@Column(name = "id_Perfil")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idPerfilSeq")
	private Integer idPerfil;

	@Column(length = 100, nullable = false)
	String descr;

	//@ManyToMany(mappedBy = "perfis")
	@ManyToMany
	@JoinTable(name = "Perfil_Usuario", foreignKey = @ForeignKey(name = "id_Perfil_FK"), joinColumns = @JoinColumn(name = "id_Perfil"), inverseJoinColumns = @JoinColumn(name = "id_Usuario"))
	private List<Usuario> usuarios;

	@ManyToMany
	@JoinTable(name = "Perfil_Menu", foreignKey = @ForeignKey(name = "id_Menu_FK"), joinColumns = @JoinColumn(name = "id_Perfil"), inverseJoinColumns = @JoinColumn(name = "id_Menu"))
	private List<Menu> menus;

	public Perfil() {
	}

	/**
	 * Perfil do usuario.
	 * 
	 * @param descr
	 */
	public Perfil(String descr) {
		this.descr = descr;
	}

	public int getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

}
