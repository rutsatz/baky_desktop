package model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Perfil_Menu")
public class PerfilMenu {

	@EmbeddedId
	private PerfilMenuPK perfilMenuPK;

	public PerfilMenu() {
	}
	
	public PerfilMenu(PerfilMenuPK perfilMenuPK){
		this.perfilMenuPK = perfilMenuPK;
	}

	public PerfilMenuPK getPerfilMenuPK() {
		return perfilMenuPK;
	}

	public void setPerfilMenuPK(PerfilMenuPK perfilMenuPK) {
		this.perfilMenuPK = perfilMenuPK;
	}

}
