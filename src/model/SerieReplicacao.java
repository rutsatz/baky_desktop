/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Rafael Rutsatz
 */
@Entity(name = "Serie_Replicacao")
public class SerieReplicacao {

    @Id
    @Column(name = "id_Serie_Replicacao")
    private Integer idSerieReplicacao;
    
    /**
     * Guarda o último número de série recebido do servidor. (Equivalente a
     * coluna serie da tabela seriereplicacao)
     */
    @Column(name = "id_Serie_Servidor")
    private Integer idSerieServidor;

    /**
     * Guarda o último número de série sincronizado com o servidor principal.
     * (Equivalente a coluna serie_a_replicar da tabela seriereplicacao)
     */
    @Column(name = "id_Serie_Sincronizar")
    private Integer idSerieSincronizar;

    /**
     * @return the idSerieReplicacao
     */
    public Integer getIdSerieReplicacao() {
        return idSerieReplicacao;
    }

    /**
     * @param idSerieReplicacao the idSerieReplicacao to set
     */
    public void setIdSerieReplicacao(Integer idSerieReplicacao) {
        this.idSerieReplicacao = idSerieReplicacao;
    }

    /**
     * @return the idSerieServidor
     */
    public Integer getIdSerieServidor() {
        return idSerieServidor;
    }

    /**
     * @param idSerieServidor the idSerieServidor to set
     */
    public void setIdSerieServidor(Integer idSerieServidor) {
        this.idSerieServidor = idSerieServidor;
    }

    /**
     * @return the idSerieSincronizar
     */
    public Integer getIdSerieSincronizar() {
        return idSerieSincronizar;
    }

    /**
     * @param idSerieSincronizar the idSerieSincronizar to set
     */
    public void setIdSerieSincronizar(Integer idSerieSincronizar) {
        this.idSerieSincronizar = idSerieSincronizar;
    }
}
