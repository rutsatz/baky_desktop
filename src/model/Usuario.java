package model;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import app.Aplicacao;
import app.JpaUtil;
import app.JpaUtil.Servidor;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;


@Entity
@SequenceGenerator(name = "idUsuarioSeq", sequenceName = "id_Usuario_Seq", allocationSize = 1)
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "usuario" }, name = "Usuario_uk") })
public class Usuario {

	@Id
	@Column(name = "id_Usuario")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idUsuarioSeq")
	private Integer idUsuario;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "Perfil_Usuario", foreignKey = @ForeignKey(name = "id_Usuario_FK"), joinColumns = @JoinColumn(name = "id_Usuario"), inverseJoinColumns = @JoinColumn(name = "id_Perfil"))
	private List<Perfil> perfis;

	@ManyToMany
	@JoinTable(name = "Usuario_Menu", foreignKey = @ForeignKey(name = "id_Usuario_FK"), joinColumns = @JoinColumn(name = "id_Usuario"), inverseJoinColumns = @JoinColumn(name = "id_Menu"))
	private List<Menu> menus;

	@Column(length = 20, nullable = false)
	private String usuario;

	@Column(nullable = true)
	private String senha;

	@Column(nullable = true)
	private String salt;

	@Column(name = "data_Cadastro")
	private LocalDateTime dataCadastro = LocalDateTime.now();

	@Version
	@Column(name = "ultima_Alteracao")
	private LocalDateTime ultimaAlteracao;

	private Boolean ativo = true;

	public Usuario() {
	}

	/**
	 * Cadastro de usuarios da aplicacao.
	 * 
	 * @param usuario
	 */
	public Usuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Adiciona os botões de menu na tela.
	 * 
	 * @param pane
	 */
	public void montarMenus(Scene scene) {

		// Carrega arquivo dos macro-menus superiores.
		Pane menuSuperior = (Pane) scene.lookup("#menuSuperior");

		Pane pane = (Pane) menuSuperior.lookup("#hbMenuPrinc");// scene.lookup("#hbMenuPrinc");
		//Pane pane = (Pane) scene.lookup("hbMenuPrinc");
		Pane menuEsquerdo = (Pane) Aplicacao.root.lookup("#menuEsquerdo");

		for (Perfil perfil : perfis) {
			List<Menu> menus = perfil.getMenus();
			for (Menu menu : menus) {

				Button bt = new Button();
				bt.setText(menu.getDescr());
				bt.setOnAction(e -> {
					String arqLayout = menu.getArquivo();
					VBox paneSubMenus = null;
					try {
						paneSubMenus = FXMLLoader.load(getClass().getResource(arqLayout));
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					addSubMenus(menu, paneSubMenus);

					Pane pilhaMenus = (Pane) Aplicacao.root.lookup("#pilhaMenus");

					int nivel = menu.getNivelSubMenu();
					int qtdExiste = pilhaMenus.getChildren().size();

					if (qtdExiste > nivel) {
						for (int x = qtdExiste; nivel < x; x--) {
							pilhaMenus.getChildren().remove(x - 1);

						}
					}

					// duplica bot�o para add na pilha de menus.
					Button btPilha = new Button();
					Button btAtual = bt;// Aplicacao.menus.peek();

					btPilha.setOnAction(btAtual.getOnAction());
					btPilha.setText(btAtual.getText());
					pilhaMenus.getChildren().add(btPilha);

				});
				pane.getChildren().add(bt);

			}
		}

	}

	private void addSubMenus(Menu menu, VBox paneSubMenus) {

		EntityManager entityManager = JpaUtil.getEntityManager(JpaUtil.getServidorAtivo());
		String consulta = "select m from Menu m where m.idMenuPai = :idPai";
		TypedQuery query = entityManager.createQuery(consulta, Menu.class);
		query.setParameter("idPai", menu);

		List<Menu> subMenus = query.getResultList();

		Pane menuEsquerdo = (Pane) Aplicacao.root.lookup("#menuEsquerdo");

		for (Menu subMenu : subMenus) {

			Button subButton = new Button();
			subButton.setText(subMenu.getDescr());
			subButton.setOnAction(e2 -> {
				String arqLayout = subMenu.getArquivo();

				Pane subPane = null;
				try {
					subPane = FXMLLoader.load(getClass().getResource(arqLayout));
				} catch (IOException e3) {
					e3.printStackTrace();
				}

				int nivel = subMenu.getNivelSubMenu();
				int qtdExiste = menuEsquerdo.getChildren().size();

				Pane pilhaMenus = (Pane) Aplicacao.root.lookup("#pilhaMenus");

				addPilhaMenus(nivel, qtdExiste, menuEsquerdo);
				qtdExiste = pilhaMenus.getChildren().size();

				if (qtdExiste > nivel) {
					for (int x = qtdExiste; nivel < x; x--) {
						Aplicacao.menus.pop();
						pilhaMenus.getChildren().remove(x - 1);

					}
				}

				addBotao(pilhaMenus, subButton);

				if (subMenu.getTemSubMenu()) {

					addSubMenus(subMenu, (VBox) subPane);
				} else {

					Aplicacao.root.setCenter(subPane);
				}
				Aplicacao.menus.push(subButton);

			});
			paneSubMenus.getChildren().add(subButton);

		}

		if (menu.getIdMenuPai() == null) {
			menuEsquerdo.getChildren().clear();
		}
		menuEsquerdo.getChildren().add(paneSubMenus);

	}

	public void addPilhaMenus(int nivel, int qtdExiste, Pane menuEsquerdo) {

		Pane pilhaMenus = (Pane) Aplicacao.root.lookup("#pilhaMenus");

		if (qtdExiste > nivel) {
			for (int x = qtdExiste; nivel < x; x--) {
				menuEsquerdo.getChildren().remove(x - 1);
				Aplicacao.menus.pop();
				pilhaMenus.getChildren().remove(x - 1);

			}
		}

	}

	public void addBotao(Pane pilhaMenus, Button btAtual) {

		// duplica bot�o para add na pilha de menus.
		Button btPilha = new Button();

		pilhaMenus.getChildren().size();

		btPilha.setOnAction(btAtual.getOnAction());
		btPilha.setText(btAtual.getText());
		pilhaMenus.getChildren().add(btPilha);

	}

	public static String criptografar(String originalPassword, String salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		int iterations = 1000;
		char[] chars = originalPassword.toCharArray();
		PBEKeySpec spec = new PBEKeySpec(chars, fromHex(salt), iterations, 64 * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = skf.generateSecret(spec).getEncoded();

		return toHex(hash);
	}

	private String generateStrongPasswordHash(String password)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		int iterations = 1000;
		char[] chars = password.toCharArray();
		byte[] salt = generateSalt();

		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = skf.generateSecret(spec).getEncoded();
		setSalt(toHex(salt));
		// setSenha(toHex(hash));
		// return iterations + ":" + toHex(salt) + ":" + toHex(hash);
		return toHex(hash);
	}

	private static byte[] generateSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt;
	}

	/**
	 * Converts a string of hexadecimal characters into a byte array.
	 *
	 * @param hex
	 *            the hex string
	 * @return the hex string decoded into a byte array
	 */
	private static byte[] fromHex(String hex) {
		byte[] binary = new byte[hex.length() / 2];
		for (int i = 0; i < binary.length; i++) {
			binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return binary;
	}

	private static String toHex(byte[] array) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalDateTime getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(LocalDateTime ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public void setSenha(String senha) {
		// this.senha = senha;
		try {
			// criptografar(senha);
			this.senha = generateStrongPasswordHash(senha);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSalt() {
		return salt;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
