/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author rutsa
 */
@Entity
@SequenceGenerator(name = "idFuncaoSeq", sequenceName = "id_Funcao_Seq")
public class Funcao {

    @Id
    @Column(name = "id_Funcao")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idFuncaoSeq")
    private Integer idFuncao;

    private String descr;
}
