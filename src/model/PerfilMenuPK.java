package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

@Embeddable
public class PerfilMenuPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "id_Menu")
	@JoinColumn(name = "id_Menu")
	private Integer idMenu;

	@Column(name = "id_Perfil")
	@JoinColumn(name = "id_Perfil")
	private Integer idPerfil;

	public PerfilMenuPK() {
	}

	public PerfilMenuPK(Integer idMenu, Integer idPerfil) {
		super();
		this.idMenu = idMenu;
		this.idPerfil = idPerfil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMenu;
		result = prime * result + idPerfil;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilMenuPK other = (PerfilMenuPK) obj;
		if (idMenu != other.idMenu)
			return false;
		if (idPerfil != other.idPerfil)
			return false;
		return true;
	}

	public Integer getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}

	public Integer getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}

}
