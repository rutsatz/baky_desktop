package controller.app;

import javafx.fxml.FXML;

import java.util.List;

import javax.persistence.EntityManager;

import app.JpaUtil;
import app.JpaUtil.Servidor;
import app.Sincronizar;
import javafx.event.ActionEvent;

import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Background;

public class StatusServidorController {
	@FXML
	private ToggleButton togbConectarServidor;

	// Event Listener on ToggleButton[#togbConectarServidor].onAction
	@FXML
	public void conectarServidor(ActionEvent event) {
		System.out.println(JpaUtil.getServidorAtivo());
		if (togbConectarServidor.isSelected()) {
			if (JpaUtil.getServidorAtivo() == Servidor.LOCAL) {

				togbConectarServidor.setText("Conectando com Servidor");

				EntityManager eml = JpaUtil.getEntityManager(Servidor.SERVIDOR);

				if (eml != null) {
					togbConectarServidor.setDisable(true);
					togbConectarServidor.setText("Aguarde... Sincronizando...");
					
					new Sincronizar().sincronizar();
					
					togbConectarServidor.setText("Conectado com Servidor");					
					togbConectarServidor.setId("toggle-conectado");
				} else {
					togbConectarServidor.setId("toggle-desconectado");
				}
			}
		}

	}
}
