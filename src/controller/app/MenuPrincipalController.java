package controller.app;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.io.IOException;

import app.Aplicacao;
import javafx.event.ActionEvent;

public class MenuPrincipalController {
	@FXML
	private HBox hbMenuPrinc;
	@FXML
	private Button btCadastro;
	@FXML
	private Button btGerenciamento;

	@FXML
	private TextField tx;
	
	// Event Listener on Button[#btCadastro].onAction
	@FXML
	public void abrirCadastro(ActionEvent event) throws IOException {
		Pane pane = FXMLLoader.load(getClass().getResource("../view/cadastro/CadastroLayout.fxml"));
		Aplicacao.root.setCenter(pane);

	}

	// Event Listener on Button[#btGerenciamento].onAction
	@FXML
	public void abrirGerenciamento(ActionEvent event) throws IOException {
		Pane pane = FXMLLoader.load(getClass().getResource("../view/app/GerenciamentoLayout.fxml"));
		Aplicacao.root.setCenter(pane);

	}
}
