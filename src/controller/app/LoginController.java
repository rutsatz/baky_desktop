package controller.app;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import app.Aplicacao;
import app.JpaUtil;
import app.Login;
import app.JpaUtil.Servidor;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import model.Usuario;

import javafx.scene.control.PasswordField;

public class LoginController {
	@FXML
	private PasswordField txSenha;
	@FXML
	private TextField txUsuario;
	@FXML
	private Button btEntrar;
	@FXML
	private Button btSair;
	@FXML
	private Label lbErro;

	@FXML
	private void initialize() {
		initTransition();
		lbErro.setVisible(false);
	}

	@FXML
	private void encerrarAplicacao() {
		JpaUtil.closeEntityManagerFactory(JpaUtil.getServidorAtivo());
		Platform.exit();
	}

	@FXML
	private void iniciarAplicao() throws IOException {
		String strUsuario = new String();
		String strPass = new String();
		strUsuario = txUsuario.getText();
		strPass = txSenha.getText();

		// Temporario para desenvolvimento. @@
		// strUsuario = "Rafael";

		// valida login e senha.
		Usuario usuario = buscarUsuario(strUsuario, strPass);
		if (usuario == null) {
			return;
		} else {

			new Aplicacao(usuario);
			Login.primaryStage.close();
		}
	}

	public Usuario buscarUsuario(String strUsuario, String Pass) {

		Usuario usuario = null;
		String consulta = "select u from Usuario u where u.ativo = true and u.usuario = :usuario";

		// try {
		// System.out.println(Usuario.criptografar("password",
		// usuario.getSalt()));
		// } catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
		// TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		EntityManager entityManager = JpaUtil.getEntityManager(JpaUtil.getServidorAtivo());
		TypedQuery<Usuario> query = entityManager.createQuery(consulta, Usuario.class);
		query.setParameter("usuario", strUsuario);
		try {
			usuario = query.getSingleResult();
		} catch (Exception e) {
			lbErro.setVisible(true);
			return null;
		}

		consulta += " and senha = :senha and salt = :salt";
		query = entityManager.createQuery(consulta, Usuario.class);
		query.setParameter("usuario", strUsuario);

		String senhaCrypt = null;

		try {
			senhaCrypt = Usuario.criptografar(txSenha.getText(), usuario.getSalt());
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvalidKeySpecException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		query.setParameter("senha", senhaCrypt);
		query.setParameter("salt", usuario.getSalt());

		try {
			usuario = query.getSingleResult();
		} catch (Exception e) {
			lbErro.setVisible(true);
			return null;
		}

		return usuario;

	}

	private void initTransition() {
		btEntrar.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				ScaleTransition transition = new ScaleTransition(Duration.millis(500), btEntrar);
				transition.setToX(1.2);
				transition.setToY(1.2);
				transition.play();
			}
		});

		btEntrar.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				ScaleTransition transition = new ScaleTransition(Duration.millis(500), btEntrar);
				transition.setToX(1.0);
				transition.setToY(1.0);
				transition.play();
			}
		});
		btSair.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				ScaleTransition transition = new ScaleTransition(Duration.millis(500), btSair);
				transition.setToX(1.2);
				transition.setToY(1.2);
				transition.play();
			}
		});

		btSair.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				ScaleTransition transition = new ScaleTransition(Duration.millis(500), btSair);
				transition.setToX(1.0);
				transition.setToY(1.0);
				transition.play();
			}
		});
	}

	public PasswordField getTxSenha() {
		return txSenha;
	}

	public TextField getTxUsuario() {
		return txUsuario;
	}

	public void setTxSenha(PasswordField txSenha) {
		this.txSenha = txSenha;
	}

	public void setTxUsuario(TextField txUsuario) {
		this.txUsuario = txUsuario;
	}
}
