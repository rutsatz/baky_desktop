package controller.cadastro;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.EntityManager;

import app.Aplicacao;
import app.JpaUtil;
import app.JpaUtil.Servidor;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import model.Pessoa;
import utils.XmlImportacao;

public class CadastroPessoaController {

    @FXML
    private Button btVoltar;

    @FXML
    private Button btSalvar;

    @FXML
    private Button btNovo;

    @FXML
    private TextField txNome;

    @FXML
    private DatePicker dpDataNascimento;

    @FXML
    private TextField txCPF;

    @FXML
    private TextField txRG;

    public CadastroPessoaController() {

    }

    @FXML
    private void voltar() throws IOException {
        Pane pane = FXMLLoader.load(getClass().getResource("../view/cadastro/CadastroLayout.fxml"));
        Aplicacao.root.setCenter(pane);

    }

    @FXML
    private void salvar() {

        LocalDate dataNascimento = dpDataNascimento.getValue();

        String nome = txNome.getText();
        String CPF = txCPF.getText();
        String RG = txRG.getText();

        Pessoa pessoa = new Pessoa();
        pessoa.setNome(nome);
        pessoa.setDataNascimento(dataNascimento);
        pessoa.setCPF(CPF);
        pessoa.setRG(RG);

        if (JpaUtil.getServidorAtivo() == Servidor.LOCAL) {
            if (!XmlImportacao.addItem(pessoa)) {
                System.out.println("Erro ao inserir no xml!");
                System.exit(1);
            }
        }
        
        EntityManager entityManager = JpaUtil.getEntityManager(JpaUtil.getServidorAtivo());
        entityManager.getTransaction().begin();

        entityManager.persist(pessoa);

        entityManager.getTransaction().commit();

        JpaUtil.closeEntityManager(JpaUtil.getServidorAtivo());

        

    }
}
