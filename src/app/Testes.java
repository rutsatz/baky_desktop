package app;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import app.JpaUtil.Servidor;
import controller.app.LoginController;
import java.math.BigInteger;
import java.time.LocalDate;
import model.Endereco;
import model.Menu;
import model.Parametro;
import model.Perfil;
import model.Pessoa;
import model.Telefone;
import model.Usuario;
import utils.XmlImportacao;

/**
 * Temporarios para testes.
 *
 * @author Raffa
 *
 */
public class Testes {

    private static Usuario usuario;

    public Testes() {

    }

    public static void cargaTabelas(LoginController c) {
        EntityManager entityManager = JpaUtil.getEntityManager(JpaUtil.getServidorAtivo());
        entityManager.getTransaction().begin();

        String bulkUpdate;
        Query query;

        bulkUpdate = "delete from Usuario s";
        query = entityManager.createQuery(bulkUpdate);
        query.executeUpdate();

        bulkUpdate = "delete from Perfil p";
        query = entityManager.createQuery(bulkUpdate);
        query.executeUpdate();

        bulkUpdate = "delete from Menu m";
        query = entityManager.createQuery(bulkUpdate);
        query.executeUpdate();

        usuario = new Usuario("t");
        usuario.setSenha("t");

        c.getTxUsuario().setText("t");
        c.getTxSenha().setText("t");

        List<Menu> menus = new LinkedList<Menu>();

        Menu menu = new Menu("Cadastro", "abrirCadastro", "/view/cadastro/CadastroLayout.fxml", true, 0);
        entityManager.persist(menu);
        menus.add(menu);

        Menu menu2 = new Menu("Produtor", "abrirCadastroPessoa", "/view/cadastro/CadastroPessoaLayout.fxml", false, 1, menu);
        entityManager.persist(menu2);
        // menus.add(menu);

        menu2 = new Menu("Usuario", "abrirCadastroUsuario", "/view/cadastro/CadastroUsuarioLayout.fxml", false, 1, menu);
        entityManager.persist(menu2);

//		menu = new Menu("Alimentacao", "abrirAlimentacao", "/view/GerenciamentoAlimentacaoLayout.fxml",true, 1, menu);
//		entityManager.persist(menu);
//		
//		Menu menu3 = new Menu("Ra��o", "abrirRacao", "/view/GerenciamentoAlimentacaoRacaoLayout.fxml",false, 2, menu);
//		entityManager.persist(menu3);
//
//		menu3 = new Menu("Pastagem", "abrirPastagem", "/view/GerenciamentoAlimentacaoRacaoLayout.fxml",false, 2, menu);
//		entityManager.persist(menu3);
//		
//		menu3 = new Menu("An�lise de Solo", "abrirAnaliseDeSolo", "/view/GerenciamentoAlimentacaoRacaoLayout.fxml",false, 2,  menu);
//		entityManager.persist(menu3);
//		
        menu = new Menu("Gerenciamento", "abrirGerenciamento", "/view/app/GerenciamentoLayout.fxml", true, 0);
        entityManager.persist(menu);
        menus.add(menu);

//		menu2 = new Menu("Alimenta��o", "abrirAlimentacao", "nao existe arquivo", menu);
//		entityManager.persist(menu2);
////
//		menu2 = new Menu("Pesagem Corporal", "abrirPesagemCorporal", "nao existe arquivo", false,1, menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Tanque de Leite", "abrirTanqueDeLeite", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Controle Leiteiro", "abrirControleLeiteiro", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Rebanho", "abrirRebanho", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Embri�o", "abrirEmbri�o", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Estoque Insumo", "abrirEstoqueInsumo", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Reprodu��o", "abrirReprodu��o", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Financeiro", "abrirFinanceiro", "nao existe arquivo", false,1,menu);
//		entityManager.persist(menu2);
//		
//		menu2 = new Menu("Sanidade", "abrirSanidade", "nao existe arquivo",false,1, menu);
//		entityManager.persist(menu2);
//				
        // menu = new Menu("Pasto", "abrirCadastroProdutor",
        // "/view/CadastroProdutorLayout.fxml");
        // entityManager.persist(menu);
        // menus.add(menu);
        Perfil perfil = new Perfil("Desenvolvedor");

        perfil.setMenus(menus);
        entityManager.persist(perfil);

        List<Perfil> perfis = new LinkedList<Perfil>();
        perfis.add(perfil);
        usuario.setPerfis(perfis);

        entityManager.persist(usuario);

        perfil = new Perfil("Assistente");
        entityManager.persist(perfil);

        // usuario.getPerfis().add(perfil);
        // entityManager.persist(usuario);
        entityManager.getTransaction().commit();

        JpaUtil.closeEntityManager(JpaUtil.getServidorAtivo());

    }

    public static void gerarXmlTeste() {

        Endereco endereco = new Endereco();
        endereco.setLogradouro("Rua");
        endereco.setEndereco("Lopes Trovão");
        endereco.setNumero(1145);
        endereco.setBairro("Centro");

        Telefone telefone = new Telefone();
        telefone.setDDD(51);
        telefone.setTelefone(997653740);
        
        Telefone celular = new Telefone();
        celular.setDDD(54);
        celular.setTelefone(989898998);
        
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("nome people");
        pessoa.setIdPessoa(2);
        pessoa.setCPF("02809520070");
        pessoa.setCk_fisica(Boolean.TRUE);
        pessoa.setDataNascimento(LocalDate.now());

        pessoa.getEnderecos().add(endereco);

        pessoa.getTelefones().add(telefone);
        pessoa.getTelefones().add(celular);
        
        XmlImportacao xmlArq = new XmlImportacao();
        xmlArq.getPessoas().add(pessoa);
        xmlArq.salvar(xmlArq);

    }

}
