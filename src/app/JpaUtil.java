package app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JpaUtil {

	public enum Servidor {
		LOCAL("PU_LOCAL"), SERVIDOR("PU_SERVER");

		private String server;

		Servidor(String server) {
			this.server = server;
		}

		public String getServer() {
			return server;
		}
	}

	private static Servidor servidorAtivo = Servidor.LOCAL;

	private static ThreadLocal<EntityManager> threadEntityManager = new ThreadLocal<EntityManager>();
	private static ThreadLocal<EntityManager> threadEntityManager2 = new ThreadLocal<EntityManager>();

	private static EntityManagerFactory entityManagerFactory;
	private static EntityManagerFactory entityManagerFactory2;

	public static EntityManager getEntityManager(Servidor server) {
		if (server == Servidor.LOCAL) {
			if (entityManagerFactory == null) {
				entityManagerFactory = Persistence.createEntityManagerFactory(server.getServer());

			}
			EntityManager entityManager = threadEntityManager.get();

			if (entityManager == null || !entityManager.isOpen()) {
				entityManager = entityManagerFactory.createEntityManager();
				JpaUtil.threadEntityManager.set(entityManager);
			}
			return entityManager;

		} else if (server == Servidor.SERVIDOR) {
			if (entityManagerFactory2 == null) {
				entityManagerFactory2 = Persistence.createEntityManagerFactory(server.getServer());

			}
			EntityManager entityManager = threadEntityManager2.get();

			if (entityManager == null || !entityManager.isOpen()) {
				entityManager = entityManagerFactory2.createEntityManager();
				JpaUtil.threadEntityManager2.set(entityManager);
			}
			return entityManager;

		}
		return null;
	}

	public static void closeEntityManager(Servidor server) {
		if (server == Servidor.LOCAL) {
			EntityManager em = threadEntityManager.get();
			if (em != null) {
				EntityTransaction transaction = em.getTransaction();
				if (transaction.isActive()) {
					transaction.commit();
				}
				em.close();
				threadEntityManager.set(null);
			}
		} else if (server == Servidor.SERVIDOR) {
			EntityManager em = threadEntityManager2.get();
			if (em != null) {
				EntityTransaction transaction = em.getTransaction();
				if (transaction.isActive()) {
					transaction.commit();
				}
				em.close();
				threadEntityManager2.set(null);
			}

		}
	}

	public static void closeEntityManagerFactory(Servidor server) {
		closeEntityManager(server);
		if (server == Servidor.LOCAL) {
			entityManagerFactory.close();
		} else if (server == Servidor.SERVIDOR) {
			entityManagerFactory2.close();
		}
	}

	public static Servidor getServidorAtivo() {
		return servidorAtivo;
	}

	public static void setServidorAtivo(Servidor server) {
		servidorAtivo = server;
	}

}