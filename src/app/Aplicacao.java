package app;

import java.io.IOException;
import java.util.Stack;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Usuario;

public class Aplicacao {

	public static Stage primaryStage = new Stage();
	public static BorderPane root;
	public static Usuario usuario;
	public static Stack<Button> menus = new Stack<>(); // Pilha dos menus da aplicacao.

	public Aplicacao(Usuario usuario) throws IOException {

		Aplicacao.usuario = usuario;
		
		root = FXMLLoader.load(getClass().getResource("/view/app/AplicacaoLayout.fxml"));

		// primaryStage.setMaximized(true);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);

		primaryStage.setTitle("Programa de Controle Leiteiro BAKY");
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/img/logo.png")));
		// primaryStage.setMinWidth(800);
		// primaryStage.setMinHeight(600);

		primaryStage.setHeight(550);
		primaryStage.setWidth(700);

		// Carrega os perfis e obt�m os menus e adiciona na tela.
		// List<Perfil> perfis = usuario.getPerfis();

		primaryStage.show();

		// Adiciona menus conforme perfil do usu�rio.
		usuario.montarMenus(scene);		
		
//		Aplicacao.root.lookup("#raizMenus").setVisible(false);
		
//		System.out.println(Aplicacao.root.lookup("#raizMenus"));
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {

				Alert dialogoExe = new Alert(Alert.AlertType.CONFIRMATION);
				ButtonType btnSim = new ButtonType("Sim");
				ButtonType btnNao = new ButtonType("Não");

				dialogoExe.setTitle("Encerrar");
				dialogoExe.setHeaderText("Deseja sair?");
				dialogoExe.getButtonTypes().setAll(btnSim, btnNao);
				dialogoExe.showAndWait().ifPresent(b -> {
					if (b == btnNao) {
						event.consume();
					} else {
						JpaUtil.closeEntityManagerFactory(JpaUtil.getServidorAtivo());
					}

				});

			}
		});
	}

}
