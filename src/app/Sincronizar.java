package app;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import app.JpaUtil.Servidor;
import javax.persistence.NoResultException;
import model.Parametro;
import model.Funcionario;

public class Sincronizar {

    public void sincronizar() {
  
        String consulta;

        EntityManager entityManager = JpaUtil.getEntityManager(Servidor.LOCAL);

        // Se é primeira utilização, parametriza tudo.
        consulta = "select p from Parametro p";
        TypedQuery<Parametro> query1 = entityManager.createQuery(consulta, Parametro.class);
        Parametro param = null;
        try {
            param = query1.getSingleResult();
        } catch (NoResultException e) {
            param = new Parametro();
//            param.setIdParam(1);
//            param.setUltimaSincronizacaoLocal(LocalDateTime.now());
//            param.setUltimaSincronizacaoServidor(LocalDateTime.now());
            entityManager.getTransaction().begin();
            entityManager.persist(param);
            entityManager.getTransaction().commit();
        }
        
        consulta = "select p from Funcionario p where p.ultimaAlteracao > :ultimaAlteracao ";
        TypedQuery<Funcionario> query = entityManager.createQuery(consulta, Funcionario.class);
//        query.setParameter("ultimaAlteracao", param.getUltimaSincronizacaoLocal());
        //query.setMaxResults(10);
        List<Funcionario> resultado = query.getResultList();

        //System.out.println(resultado.get(0));
        EntityManager entityManager2 = JpaUtil.getEntityManager(Servidor.SERVIDOR);

        entityManager2.getTransaction().begin();

        for (Funcionario funcionario : resultado) {
//			entityManager2.persist(produtor);
            entityManager2.merge(funcionario);
//			entityManager2.flush();
//			entityManager2.clear();
        }

        entityManager2.getTransaction().commit();

        JpaUtil.closeEntityManager(Servidor.SERVIDOR);

//        param.setUltimaSincronizacaoLocal(LocalDateTime.now());
        entityManager.persist(param);

    }

}
