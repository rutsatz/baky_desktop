package app;

import java.io.IOException;

import app.JpaUtil.Servidor;
import controller.app.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Pessoa;
import model.Usuario;
import utils.XmlImportacao;

public class Login extends Application {

	public static Stage primaryStage;
	private double dragOffsetX;
	private double dragOffsetY;

	@Override
	public void start(Stage primaryStage) throws IOException {

		Usuario usuario;
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/app/LoginLayout.fxml"));

		// Painel principal
		Pane root = loader.load();

		LoginController controller = loader.getController();

//		JpaUtil.setServidorAtivo(Servidor.LOCAL);

		Testes.cargaTabelas(controller);

                
                
                //Testes.gerarXmlTeste();
                
                
		// JpaUtil.closeEntityManagerFactory();

		//new Sincronizar().sincronizar();
		
		Scene scene = new Scene(root, 400, 400);
		scene.setOnMousePressed(e -> handleMousePressed(e));
		scene.setOnMouseDragged(e -> handleMouseDragged(e));
		primaryStage.setScene(scene);

		primaryStage.setTitle("Programa de Controle Leiteiro BAKY");
		primaryStage.setHeight(400);
		primaryStage.setWidth(500);
		primaryStage.setResizable(false);
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/img/logo.png")));

		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.setOpacity(0.971);
		primaryStage.show();
		Login.primaryStage = primaryStage;

	}

	protected void handleMousePressed(MouseEvent e) {
		// Store the mouse x and y coordinates with respect to the
		// stage in the reference variables to use them in the drag event
		this.dragOffsetX = e.getScreenX() - primaryStage.getX();
		this.dragOffsetY = e.getScreenY() - primaryStage.getY();
	}

	protected void handleMouseDragged(MouseEvent e) {
		// Move the stage by the drag amount
		primaryStage.setX(e.getScreenX() - this.dragOffsetX);
		primaryStage.setY(e.getScreenY() - this.dragOffsetY);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
